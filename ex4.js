/**
 * Input
 * var canh1, canh2, canh3
 *
 * Todo
 * Dom lấy canh1,canh2,canh3
 * su dung elseif so sanh trả ket qua
 *
 *
 * Result
 * Tra ket qua khi user nhấn btn
 *
 *
 */
function duDoan() {
  var a = document.getElementById("canh-1").value * 1;
  var b = document.getElementById("canh-2").value * 1;
  var c = document.getElementById("canh-3").value * 1;
  console.log({ a, b, c });

  if (a == b && b == c) {
    result = `Tam giác đều`;
  } else if (
    (a == b && a != c) ||
    (a == c && a != b) ||
    (b == c && b != a) ||
    (b == a && b != c) ||
    (c == a && c != b) ||
    (c == b && c != a)
  ) {
    result = `Tam giác cân`;
  } else if (
    a * a == b * b + c * c ||
    b * b == a * a + c * c ||
    c * c == a * a + b * b
  ) {
    result = `Tam giác vuông`;
  } else {
    result = `Tam giác khác`;
  }

  document.getElementById("result").innerText = result;
}
