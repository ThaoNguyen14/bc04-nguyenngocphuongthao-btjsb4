/**
 * Input
 * var user : Bố, Mẹ, Anh, Em
 *
 * Todo
 * Dom lấy value user
 * var result
 * su dung else if so sanh
 *
 * Result
 * tra result tương ứng với user nhập khi user chọn user trg ô input và bấm btn
 *
 */

function guiLoiChao() {
  //   console.log("yes");
  var user = document.getElementById("user").value;
  console.log("user: ", user);

  if (user == "B") {
    result = "Xin chào Bố!";
  } else if (user == "M") {
    result = "Xin chào Mẹ!";
  } else if (user == "A") {
    result = "Xin chào Anh!";
  } else if (user == "E") {
    result = "Xin chào Em!";
  } else {
    result = "Vui lòng chọn người dùng!";
  }

  document.getElementById("result").innerText = result;
}
