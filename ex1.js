/**
 * Input
 * var a
 * var b
 * var c
 *
 * Todo
 * Dom a,b,c
 * var result
 * su dung else-if so sanh;
 *
 * Output
 * tra result khi user bam btn Sap Xep
 */

function sapXep() {
  var a = document.getElementById("a").value * 1;
  var b = document.getElementById("b").value * 1;
  var c = document.getElementById("c").value * 1;
  console.log({ a, b, c });

  if (a > b && b > c) {
    result = `${a} > ${b} > ${c}`;
  } else if (a > c && c > b) {
    result = `${a} > ${c} > ${b}`;
  } else if (b > a && a > c) {
    result = `${b} > ${a} > ${c}`;
  } else if (b > c && c > a) {
    result = `${b} > ${c} > ${a}`;
  } else if (c > a && a > b) {
    result = `${c} > ${a} > ${b}`;
  } else {
    result = `${c} > ${b} > ${a}`;
  }

  document.getElementById("result").innerText = result;
}
